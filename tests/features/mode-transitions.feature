Feature: Dish master mode transition acceptance tests

	@SKA_mid @acceptance
	Scenario Outline: Test dish master dishMode and pointingState transitions
		Given <dish_master> reports <initial_dish_mode> dish mode
		When I request <dish_master> transition to <desired_dish_mode>
		Then <dish_master> dishMode reports <desired_dish_mode>
		And <dish_master> pointingState reports <pointing_state>
		And <dish_master> state reports <dish_state>

		Examples:
		| dish_master          | initial_dish_mode | desired_dish_mode | pointing_state | dish_state |
		| mid_d0001/elt/master | STANDBY_LP        | STANDBY_FP        | NONE           | STANDBY    |
		| mid_d0001/elt/master | STANDBY_FP        | OPERATE           | READY          | ON         |
		| mid_d0001/elt/master | OPERATE           | STANDBY_FP        | NONE           | STANDBY    |
		| mid_d0001/elt/master | STANDBY_FP        | STOW              | NONE           | DISABLE    |
		| mid_d0001/elt/master | STANDBY_FP        | STANDBY_LP        | NONE           | STANDBY    |
