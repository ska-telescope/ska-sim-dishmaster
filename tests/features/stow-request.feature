Feature: Dish master stow acceptance tests

	@SKA_mid @acceptance
	Scenario: Test dish stow request
		Given dish master reports any allowed dishMode
		When I execute a stow command
		Then the dishMode should report STOW
		And the elevation should be almost equal to 85
		And the azimuth should remain in the same position
		And the pointingState should be NONE
		And the dish state should be DISABLE
