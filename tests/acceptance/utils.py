import time

from assertpy import assert_that
from tango import CmdArgType

_MODE_CMD_MAP = {
    "STANDBY_LP": "SetStandbyLPMode",
    "STANDBY_FP": "SetStandbyFPMode",
    "OPERATE": "SetOperateMode",
    "STOW": "SetStowMode",
}


def wait_for_change_on_resource(dev_proxy, attr_name, initial_val, timeout=3):
    """Wait a while for a change in an attribute value"""
    checkpoint = timeout / 4
    start_time = time.time()
    time_elapsed = 0

    while time_elapsed <= timeout:
        current_val = retrieve_attr_value(dev_proxy, attr_name)
        if current_val != initial_val:
            break

        if (time_elapsed % checkpoint) == 0:
            print(f"Waiting for {attr_name} to change value")
            time.sleep(0.8 * checkpoint)
        time_elapsed = int(time.time() - start_time)


def retrieve_attr_value(dev_proxy, attr_name):
    """Get the attribute reading from device"""
    current_val = dev_proxy.read_attribute(attr_name)
    if current_val.type == CmdArgType.DevEnum:
        current_val = getattr(dev_proxy, attr_name).name
    elif current_val.type == CmdArgType.DevState:
        current_val = str(current_val.value)
    else:
        current_val = current_val.value
    return current_val


def request_mode_change(dev_proxy, desired_mode):
    """Transition device to requested dish mode"""
    initial_val = retrieve_attr_value(dev_proxy, "dishMode")
    dev_proxy.command_inout(_MODE_CMD_MAP[desired_mode])
    wait_for_change_on_resource(dev_proxy, "dishMode", initial_val)


def dish_mode_pre_condition(dev_proxy, expected_dish_mode):
    """verify the device dish mode before executing mode transition requests"""
    current_dish_mode = retrieve_attr_value(dev_proxy, "dishMode")
    if current_dish_mode != expected_dish_mode:
        # standbyfp is used as initial condition because it can be reached
        # from other dish modes but dont request standbyfp if this is the
        # current dish mode
        if current_dish_mode != "STANDBY_FP":
            request_mode_change(dev_proxy, "STANDBY_FP")
        request_mode_change(dev_proxy, expected_dish_mode)
        current_dish_mode = retrieve_attr_value(dev_proxy, "dishMode")
    assert_that(current_dish_mode).is_equal_to(expected_dish_mode)
