# -*- coding: utf-8 -*-
"""
override class with command handlers for dsh-lmc.
"""
# Standard python imports
import enum
import logging
from collections import namedtuple

# Additional import
from ska_ser_logging import configure_logging

# Tango import
from tango import DevState, ErrSeverity, Except

configure_logging()
MODULE_LOGGER = logging.getLogger(__name__)

AzEl = namedtuple("AzEl", ["azim", "elev"])


class OverrideDish:  # pylint: disable=too-many-public-methods
    """
    Dish master behaviour
    """

    TS_IDX = 0
    AZIM_IDX = 1
    ELEV_IDX = 2
    # az & el limits for desired/achieved pointing
    MAINT_AZIM = 90.0
    STOW_ELEV_POSITION = 85.0
    MAX_DESIRED_AZIM = 270.0
    MIN_DESIRED_AZIM = -270.0
    MAX_DESIRED_ELEV = 90.0
    MIN_DESIRED_ELEV = 15.0
    # unit for drive rate in degrees per second
    AZIM_DRIVE_MAX_RATE = 3.0
    ELEV_DRIVE_MAX_RATE = 1.0
    # ack code interpretation
    OK = 0
    FAILED = 2
    # limit on number of desiredPointing samples to keep
    # (calls to pre_update happen once per second)
    MAX_SAMPLE_HISTORY = 2400

    # initialise positions to match achievedPointing and
    # desiredPointing values in ska_mpi_dsh_lmc.fgo
    requested_position = AzEl(azim=0.0, elev=30.0)
    actual_position = AzEl(azim=0.0, elev=30.0)
    desired_pointings = []

    # Latest update between programTrackTable and desiredPointing
    last_coordinate_update_timestamp = 0.0

    @staticmethod
    def _configureband(model, band_number):
        _allowed_modes = ("STANDBY_FP", "OPERATE", "STOW")
        ds_indexer_position = model.sim_quantities["dsIndexerPosition"]
        configured_band = model.sim_quantities["configuredBand"]
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)
        if dish_mode in _allowed_modes:
            set_enum(dish_mode_quantity, "CONFIG", model.time_func())
            model.logger.info(
                f"Configuring DISH to operate in frequency band {band_number}."
            )

            # TODO (p.dube 19-05-2021) Implement sleep in a background
            # thread to allow the dishMode to remain in 'CONFIG' to
            # simulate the real DSH LMC.

            set_enum(
                ds_indexer_position,
                f"B{band_number}",
                model.time_func(),
            )
            set_enum(configured_band, f"B{band_number}", model.time_func())
            model.logger.info(
                f"Done configuring DISH to operate in"
                f" frequency band {band_number}."
            )
            model.logger.info(f"DISH reverting back to '{dish_mode}' mode.")
            set_enum(dish_mode_quantity, dish_mode, model.time_func())
        else:
            Except.throw_exception(
                "DISH Command Failed",
                f"DISH is not in {_allowed_modes} mode.",
                "ConfigureBand{band_number}()",
                ErrSeverity.WARN,
            )

    def action_configureband1(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 1. On completion of the band
        configuration, Dish will automatically revert to the previous
        Dish mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "1")

    def action_configureband2(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 2. On completion of the band
        configuration, Dish will automatically revert to the previous
        Dish mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "2")

    def action_configureband3(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 3. On completion of the band
        configuration, Dish will automatically revert to the previous Dish
        mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "3")

    def action_configureband4(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE Dish
        Element Mode, and returns to the caller. To configure the Dish to
        operate in frequency band 4. On completion of the band configuration,
        Dish will automatically revert to the previous Dish mode
        (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "4")

    def action_configureband5a(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 5a. On completion of the band
        configuration, Dish will automatically revert to the previous Dish
        mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "5a")

    def action_configureband5b(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 5b. On completion of the band
        configuration, Dish will automatically revert to the previous Dish
        mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "5b")

    def action_configureband5c(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the CONFIGURE
        Dish Element Mode, and returns to the caller. To configure the
        Dish to operate in frequency band 5c. On completion of the band
        configuration, Dish will automatically revert to the previous Dish
        mode (OPERATE or STANDBY_FP).

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP, OPERATE, STOW).
        """
        self._configureband(model, "5c")

    @staticmethod
    def _throw_exception(command, allowed_modes):
        Except.throw_exception(
            "DISH Command Failed",
            f"DISH is not in {allowed_modes} mode.",
            f"{command}()",
            ErrSeverity.WARN,
        )

    def action_lowpower(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the LOW power
        state. All subsystems go into a low power state to power only the
        essential equipment. Specifically the Helium compressor will be set
        to a low power consumption, and the drives will be disabled. When
        issued a STOW command while in LOW power, the DS controller
        should be able to turn the drives on, stow the dish and turn the
        drives off again. The purpose of this mode is to enable the
        observatory to perform power management (load curtailment), and
        also to conserve energy for non-operating dishes.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STOW, MAINTENANCE).
        """
        _allowed_modes = ("STOW", "MAINTENANCE")
        dish_mode = get_enum_str(model.sim_quantities["dishMode"])
        if dish_mode in _allowed_modes:
            set_enum(
                model.sim_quantities["powerState"], "LOW", model.time_func()
            )
            model.logger.info("Dish transitioning to 'LOW' power state.")
        else:
            self._throw_exception("LowPower", _allowed_modes)

    @staticmethod
    def _reset_pointing_state(model):
        action = "NONE"
        pointing_state_quantity = model.sim_quantities["pointingState"]
        pointing_state = get_enum_str(pointing_state_quantity)
        if pointing_state != action:
            model.logger.info(f"Current pointingState is {pointing_state}.")
            set_enum(pointing_state_quantity, action, model.time_func())
            model.logger.info("pointingState reset to 'NONE'.")
        else:
            model.logger.warning(f"pointingState is already '{action}'.")

    def action_setmaintenancemode(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the MAINTENANCE
        Dish Element Mode, and returns to the caller. To go into a state that
        is safe to approach the Dish by a maintainer, and to enable the
        Engineering interface to allow direct access to low level control and
        monitoring by engineers and maintainers. This mode will also enable
        engineers and maintainers to upgrade SW and FW. Dish also enters
        this mode when an emergency stop button is pressed.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_LP, STANDBY_FP).
        """
        maintenance = "MAINTENANCE"
        _allowed_modes = ("STANDBY_LP", "STANDBY_FP")
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode == maintenance:
            model.logger.info(f"Dish is already in {maintenance} mode")
            return

        if dish_mode in _allowed_modes:
            elev = self.MIN_DESIRED_ELEV
            desiredPointing = [0.0] * len(
                model.sim_quantities["desiredPointing"].last_val
            )
            desiredPointing[self.TS_IDX] = model.time_func()
            desiredPointing[self.AZIM_IDX] = self.MAINT_AZIM
            desiredPointing[self.ELEV_IDX] = elev
            model.sim_quantities["desiredPointing"].set_val(
                desiredPointing, model.time_func()
            )
            set_enum(dish_mode_quantity, maintenance, model.time_func())
            model.logger.info(f"Dish transitioned to the {maintenance} mode.")
            self._reset_pointing_state(model)
        else:
            self._throw_exception("SetMaintenanceMode", _allowed_modes)

        tango_dev.set_state(DevState.DISABLE)
        model.logger.info("Dish state set to 'DISABLE'.")

    def action_setoperatemode(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the OPERATE Dish
        Element Mode, and returns to the caller. This mode fulfils the main
        purpose of the Dish, which is to point to designated directions while
        capturing data and transmitting it to CSP.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_FP).
        """
        operate = "OPERATE"
        _allowed_modes = ("STANDBY_FP",)
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode == operate:
            model.logger.info(f"Dish is already in {operate} mode")
            return

        if dish_mode in _allowed_modes:
            configuredBand = model.sim_quantities["configuredBand"]
            band_error_labels = ["NONE", "UNKNOWN", "ERROR", "UNDEFINED"]
            if configuredBand in band_error_labels:
                Except.throw_exception(
                    "DISH Command Failed",
                    f"Configured band is {configuredBand}.",
                    "SetOperateMode()",
                    ErrSeverity.WARN,
                )
            set_enum(dish_mode_quantity, operate, model.time_func())
            model.logger.info(
                f"Dish transitioned to the {operate} Dish Element Mode."
            )
            pointing_state_quantity = model.sim_quantities["pointingState"]
            set_enum(pointing_state_quantity, "READY", model.time_func())
            model.logger.info("Dish pointing state set to 'READY'.")
        else:
            self._throw_exception("SetOperateMode", _allowed_modes)

        tango_dev.set_state(DevState.ON)
        model.logger.info("Dish state set to 'ON'.")

    def action_setstandbyfpmode(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the STANDBY_FP Dish
        Element Mode, and returns to the caller. To prepare all subsystems
        for active observation, once a command is received by TM to go to the
        FULL_POWER mode.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (STANDBY_LP, STOW, OPERATE, MAINTENANCE).
        """
        standby_fp = "STANDBY_FP"
        _allowed_modes = ("STANDBY_LP", "STOW", "OPERATE", "MAINTENANCE")
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode == standby_fp:
            model.logger.info(f"Dish is already in {standby_fp} mode")
            return

        if dish_mode in _allowed_modes:
            set_enum(dish_mode_quantity, standby_fp, model.time_func())
            model.logger.info(
                f"Dish transitioned to the {standby_fp} Dish Element Mode."
            )
            self._reset_pointing_state(model)
        else:
            self._throw_exception("SetStandbyFPMode", _allowed_modes)

        tango_dev.set_state(DevState.STANDBY)
        model.logger.info("Dish state set to 'STANDBY'.")

    def action_setstandbylpmode(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the STANDBY_LP
        Dish Element Mode, and returns to the caller. Standby_LP is the
        default mode when the Dish is configured for low power consumption,
        and is the mode wherein Dish ends after a start up procedure.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OFF, STARTUP, SHUTDOWN, STANDBY_FP,
            MAINTENANCE, STOW, CONFIG, OPERATE).
        """
        standby_lp = "STANDBY_LP"
        _allowed_modes = (
            "OFF",
            "STARTUP",
            "SHUTDOWN",
            "STANDBY_FP",
            "MAINTENANCE",
            "STOW",
            "CONFIG",
            "OPERATE",
        )
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode == standby_lp:
            model.logger.info(f"Dish is already in {standby_lp} mode")
            return

        if dish_mode in _allowed_modes:
            set_enum(dish_mode_quantity, standby_lp, model.time_func())
            model.logger.info(
                f"Dish transitioned to the {standby_lp} Dish Element Mode."
            )
            self._reset_pointing_state(model)
        else:
            self._throw_exception("SetStandbyLPMode", _allowed_modes)

        tango_dev.set_state(DevState.STANDBY)
        model.logger.info("Dish state set to 'STANDBY'.")

    def action_setstowmode(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """This command triggers the Dish to transition to the STOW Dish
        Element Mode, and returns to the caller. To point the dish in a
        direction that minimises the wind loads on the structure, for survival
        in strong wind conditions. The Dish is able to observe in the stow
        position, for the purpose of transient detection.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OFF, STARTUP, SHUTDOWN, STANDBY_LP,
            STANDBY_FP, MAINTENANCE, CONFIG, OPERATE).
        """
        stow = "STOW"
        _allowed_modes = (
            "OFF",
            "STARTUP",
            "SHUTDOWN",
            "STANDBY_LP",
            "STANDBY_FP",
            "MAINTENANCE",
            "CONFIG",
            "OPERATE",
        )
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode == stow:
            model.logger.info(f"Dish is already in {stow} mode")
            return

        if dish_mode in _allowed_modes:
            # movement to stow position is handled in find_next_position
            set_enum(dish_mode_quantity, stow, model.time_func())
            model.logger.info(
                f"Dish transitioned to the {stow} Dish Element Mode."
            )
            self._reset_pointing_state(model)
        else:
            self._throw_exception("SetStowMode", _allowed_modes)

        tango_dev.set_state(DevState.DISABLE)
        model.logger.info("Dish state set to 'DISABLE'.")

    def action_startcapture(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """Triggers the dish to start capturing the data on the
        configured band.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OPERATE) or configuredBand is (NONE, UNKNOWN, ERROR, UNDEFINED).
        """
        _allowed_modes = ("OPERATE",)
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)

        if dish_mode in _allowed_modes:
            configuredBand = model.sim_quantities["configuredBand"]
            band_error_labels = ["NONE", "UNKNOWN", "ERROR", "UNDEFINED"]
            if configuredBand in band_error_labels:
                Except.throw_exception(
                    "DISH Command Failed",
                    "configuredBand is {configuredBand}.",
                    "StartCapture()",
                    ErrSeverity.WARN,
                )
            model.sim_quantities["capturing"].set_val(True, model.time_func())
            model.logger.info("Attribute 'capturing' set to True.")
        else:
            self._throw_exception("StartCapture", _allowed_modes)

        model.logger.info("'StartCapture' command executed successfully.")

    def action_stopcapture(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """Triggers the dish to stop capturing the data on the configured band.

        :param model: tango_simlib.model.Model
        :param data_input: None
        """
        if model.sim_quantities["capturing"]:
            model.sim_quantities["capturing"].set_val(False, model.time_func())
            model.logger.info("Attribute 'capturing' set to False.")

        self._change_pointing_state(model, "READY", ("OPERATE",))
        model.logger.info("'StopCapture' command executed successfully.")

    def _change_pointing_state(self, model, action, allowed_modes):
        dish_mode_quantity = model.sim_quantities["dishMode"]
        dish_mode = get_enum_str(dish_mode_quantity)
        if dish_mode not in allowed_modes:
            self._throw_exception(action, allowed_modes)

        pointing_state_quantity = model.sim_quantities["pointingState"]
        pointing_state = get_enum_str(pointing_state_quantity)
        if pointing_state != action:
            set_enum(pointing_state_quantity, action, model.time_func())
            model.logger.info(f"Dish pointingState set to {action}.")
        else:
            model.logger.warning(f"pointingState is already '{action}'.")

    def action_track(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """The Dish is tracking the commanded pointing positions within the
        specified TRACK pointing accuracy.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OPERATE).
        """
        # pointing state is changed to TRACK
        # when dish is in the requested position
        self._change_pointing_state(model, "SLEW", ("OPERATE",))
        model.logger.info("'Track' command executed successfully.")

    def action_trackstop(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """The Dish will stop tracking but will not apply brakes.
        Stops movement, but doesn't clear tables/queues.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OPERATE).
        """
        self._change_pointing_state(model, "READY", ("OPERATE",))
        model.logger.info("'TrackStop' command executed successfully.")

    @staticmethod
    def action_resettracktable(
        model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """Resets the coordinates in the queue. Clear ACU's table
        (should show number of coordinates drops to zero)

        :param model: tango_simlib.model.Model
        :param data_input: None
        """
        program_track_quantity = model.sim_quantities["programTrackTable"]
        track_table_size = len(program_track_quantity.last_val)
        default_values = [0.0] * track_table_size
        model.sim_quantities["programTrackTable"].set_val(
            default_values, model.time_func()
        )
        model.logger.info("'ResetTrackTable' command executed successfully.")

    def action_resettracktablebuffer(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """Resets the Dish LMC's buffer. (In our case it's desired_pointings)

        :param model: tango_simlib.model.Model
        :param data_input: None
        """
        self.desired_pointings = []
        model.logger.info(
            "'ResetTrackTableBuffer' command executed successfully."
        )

    def action_slew(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """The Dish moves to the commanded pointing angle at the maximum
        speed, as defined by the specified slew rate.

        :param model: tango_simlib.model.Model
        :param data_input: list
            [0]: Azimuth
            [1]: Elevation
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OPERATE).
        """
        # TODO (KM 19-11-2020) Set the data_input to desiredPointing
        self._change_pointing_state(model, "SLEW", ("OPERATE",))
        model.logger.info("'Slew' command executed successfully.")

    def action_scan(
        self, model, tango_dev=None, data_input=None
    ):  # pylint: disable=W0613
        """The Dish is tracking the commanded pointing positions within the
        specified SCAN pointing accuracy.

        :param model: tango_simlib.model.Model
        :param data_input: None
        :raises DevFailed: dishMode is not in any of the allowed modes
            (OPERATE).
        """
        self._change_pointing_state(model, "SCAN", ("OPERATE",))
        model.logger.info("'Scan' command executed successfully.")

    def find_next_position(self, desired_pointings, model, sim_time):
        """Return the latest desiredPointing not in
        the future, or last requested.
        """
        best_pointing = None
        dish_mode = get_enum_str(model.sim_quantities["dishMode"])
        # move to stow position regardless of timestamp
        if dish_mode == "STOW":
            return AzEl(
                azim=self.actual_position.azim, elev=self.STOW_ELEV_POSITION
            )
        for pointing in desired_pointings:
            timestamp = pointing[self.TS_IDX] / 1000.0  # convert ms to sec
            if timestamp <= sim_time:
                best_pointing = pointing
            else:
                break  # all other samples are in the future
        if best_pointing is not None:
            return AzEl(
                azim=best_pointing[self.AZIM_IDX],
                elev=best_pointing[self.ELEV_IDX],
            )
        # no useful samples, so use last requested position
        return self.requested_position

    @staticmethod
    def is_movement_allowed(model):
        """Determine dish is in the required pointing state and dish mode"""
        pointing_state = get_enum_str(model.sim_quantities["pointingState"])
        dish_mode = get_enum_str(model.sim_quantities["dishMode"])
        return (
            pointing_state in ["SLEW", "TRACK", "SCAN"] or dish_mode == "STOW"
        )

    def is_on_target(self):
        """Determine dish is on the requested target"""
        actual = self.actual_position
        target = self.requested_position
        return self._almost_equal(
            actual.azim, target.azim
        ) and self._almost_equal(actual.elev, target.elev)

    def update_movement_attributes(self, model, sim_time):
        """Update lock, achieved_pointing and pointing_states attributes"""
        self.set_lock_attribute(model, self.is_on_target())
        self.set_achieved_pointing_attribute(
            model, sim_time, self.actual_position
        )
        self.set_track_pointing_state_on_target(model)

    @staticmethod
    def set_lock_attribute(model, target_reached):
        """Determine lock attribute from desired and actual position"""
        target_lock = model.sim_quantities["targetLock"]
        if target_lock.last_val != target_reached:
            target_lock.last_val = target_reached
            model.logger.info(
                f"Attribute 'targetLock' set to {target_reached}"
            )

    def set_track_pointing_state_on_target(self, model):
        """Update pointing state to TRACK on target"""
        pointing_state = get_enum_str(model.sim_quantities["pointingState"])
        target_lock = model.sim_quantities["targetLock"].last_val
        # update the pointing state to TRACK when the dish arrives on target
        if target_lock and pointing_state == "SLEW":
            self._change_pointing_state(model, "TRACK", ("OPERATE",))

    def set_achieved_pointing_attribute(self, model, sim_time, position):
        """Update achieved pointing with new az, el, timestamp values"""
        achievedPointing = [0, 0, 0]
        achievedPointing[self.TS_IDX] = (
            sim_time * 1000.0
        )  # millisecond timestamp
        achievedPointing[self.AZIM_IDX] = position.azim
        achievedPointing[self.ELEV_IDX] = position.elev
        model.sim_quantities["achievedPointing"].set_val(
            achievedPointing, sim_time
        )

    def get_rate_limited_position(self, current_pos, next_pos, dt):
        """Determine the AzEl pointing from current and next positions"""
        # calc required deltas in az and el
        required_delta_azim = abs(current_pos.azim - next_pos.azim)
        required_delta_elev = abs(current_pos.elev - next_pos.elev)

        # calc max deltas in az and el due to speed limits
        max_slew_azim = self.AZIM_DRIVE_MAX_RATE * dt
        max_slew_elev = self.ELEV_DRIVE_MAX_RATE * dt

        # limit
        allowed_delta_azim = min(max_slew_azim, required_delta_azim)
        allowed_delta_elev = min(max_slew_elev, required_delta_elev)

        # get direction signs: +1 or -1
        sign_azim = get_direction_sign(current_pos.azim, next_pos.azim)
        sign_elev = get_direction_sign(current_pos.elev, next_pos.elev)

        return AzEl(
            azim=(current_pos.azim + sign_azim * allowed_delta_azim),
            elev=(current_pos.elev + sign_elev * allowed_delta_elev),
        )

    def ensure_within_mechanical_limits(self, next_pos):
        """Determine the dish az and el are in the mechanical limits"""
        if (
            next_pos.azim > self.MAX_DESIRED_AZIM
            or next_pos.azim < self.MIN_DESIRED_AZIM
        ):
            pointing_limits = [self.MIN_DESIRED_AZIM, self.MAX_DESIRED_AZIM]
            desc = (
                f"Desired azimuth angle {next_pos.azim} is out of"
                f" pointing limits {pointing_limits}."
            )
            Except.throw_exception(
                "Skipping dish movement.",
                desc,
                "ensure_within_mechanical_limits()",
                ErrSeverity.WARN,
            )
        elif (
            next_pos.elev > self.MAX_DESIRED_ELEV
            or next_pos.elev < self.MIN_DESIRED_ELEV
        ):
            pointing_limits = [self.MIN_DESIRED_ELEV, self.MAX_DESIRED_ELEV]
            desc = (
                f"Desired elevation angle {next_pos.elev} is out of"
                f" pointing limits {pointing_limits}."
            )
            Except.throw_exception(
                "Skipping dish movement.",
                desc,
                "ensure_within_mechanical_limits()",
                ErrSeverity.WARN,
            )

    def move_towards_target(self, model, sim_time, dt):
        """Move the dish towards the desired position"""
        next_requested_pos = self.find_next_position(
            self.desired_pointings, model, sim_time
        )
        self.requested_position = next_requested_pos

        self.ensure_within_mechanical_limits(next_requested_pos)
        next_achievable_pos = self.get_rate_limited_position(
            self.actual_position, next_requested_pos, dt
        )
        self.actual_position = next_achievable_pos

    def get_new_unverified_pointings(self, model):
        """Return the latest list of coordinates

        :param model: Model
            The device Model

        :return: list
            - Empty if no updates have occured since the last time
            - 1 entry of desiredPointing if it is the latest
            - All the entries of programTrackTable if it is the latest
            (7 in testing)
        """
        programTrackTable_last_update = model.sim_quantities[
            "programTrackTable"
        ].last_update_time
        desiredPointing_last_update = model.sim_quantities[
            "desiredPointing"
        ].last_update_time

        if programTrackTable_last_update > desiredPointing_last_update:
            if (
                programTrackTable_last_update
                > self.last_coordinate_update_timestamp
            ):
                self.last_coordinate_update_timestamp = (
                    programTrackTable_last_update
                )
                all_values = model.sim_quantities["programTrackTable"].last_val
                assert len(all_values) % 3 == 0, (
                    "Length of programTrackTable should ",
                    "be divisble by 3",
                )
                # Group in 3s
                return list(map(list, zip(*(iter(all_values),) * 3)))
        else:
            if (
                desiredPointing_last_update
                > self.last_coordinate_update_timestamp
            ):
                self.last_coordinate_update_timestamp = (
                    desiredPointing_last_update
                )
                return [model.sim_quantities["desiredPointing"].last_val]
        return []

    def get_new_valid_pointings(self, model):
        """Calculate valid pointings"""
        unverified_pointings = self.get_new_unverified_pointings(model)
        now_millisec = model.time_func() * 1000.0
        return [
            pointing
            for pointing in unverified_pointings
            if pointing[self.TS_IDX] >= now_millisec
        ]

    def update_desired_pointing_history(self, model):
        """Keep desired pointing to max sample size"""
        latest_pointings = self.get_new_valid_pointings(model)
        self.desired_pointings.extend(latest_pointings)
        if len(self.desired_pointings) > self.MAX_SAMPLE_HISTORY:
            # drop older samples
            self.desired_pointings = self.desired_pointings[
                -self.MAX_SAMPLE_HISTORY :  # noqa E203
            ]

    def pre_update(self, model, sim_time, dt):
        """Pre-update method used by tango-simlib"""
        if self.is_movement_allowed(model):
            self.update_desired_pointing_history(model)
            self.move_towards_target(model, sim_time, dt)
            self.update_movement_attributes(model, sim_time)
        else:
            model.logger.debug(
                "Skipping quantity updates - movement not allowed"
            )

    @staticmethod
    def _almost_equal(x, y, abs_threshold=5e-3):
        """Takes two values return true if they are almost equal"""
        return abs(x - y) <= abs_threshold


def get_enum_str(quantity):
    """Returns the enum label of an enumerated data type

    :param quantity: object
        The quantity object of a DevEnum attribute
    :return: str
        Current string value of a DevEnum attribute
    """
    EnumClass = enum.IntEnum(
        "EnumLabels", quantity.meta["enum_labels"], start=0
    )
    return EnumClass(quantity.last_val).name


def set_enum(quantity, label, timestamp):
    """Sets the quantity last_val attribute to index of label

    :param quantity: object
        The quantity object from model
    :param label: str
        The desired label from enum list
    :param timestamp: float
        The time now
    """
    value = quantity.meta["enum_labels"].index(label)
    quantity.set_val(value, timestamp)


def get_direction_sign(here, there):
    """Return sign (+1 or -1) required to move from here to there."""
    return 1 if here < there else -1
