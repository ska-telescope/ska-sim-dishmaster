# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL=/bin/bash
.SHELLFLAGS=-o pipefail -c

NAME=ska-sim-dishmaster

VERSION=$(shell grep -e "^version = s*" pyproject.toml | cut -d = -f 2 | xargs)
IMAGE=$(CAR_OCI_REGISTRY_HOST)/$(NAME)
DOCKER_BUILD_CONTEXT=.
DOCKER_FILE_PATH=Dockerfile

MINIKUBE ?= true ## Minikube or not
TANGO_HOST ?= tango-databaseds:10000  ## TANGO_HOST connection to the Tango DS

# Set the specific environment variables required for pytest
ifeq ($(MAKECMDGOALS),python-test)
MARK = not acceptance
endif

ifeq ($(MAKECMDGOALS),k8s-test-runner)
MARK = acceptance
TANGO_HOST = tango-databaseds.$(KUBE_NAMESPACE).svc.cluster.local:10000
endif

PYTHON_VARS_BEFORE_PYTEST ?= TANGO_HOST=$(TANGO_HOST) PYTHONPATH=.:./src
PYTHON_VARS_AFTER_PYTEST ?= -m '$(MARK)' --forked --json-report --json-report-file=build/report.json --junitxml=build/report.xml

# k8s variables
OCI_TAG = $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
CI_REGISTRY ?= registry.gitlab.com

ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set dishmaster.image.image=$(NAME) \
	--set dishmaster.image.registry=$(CI_REGISTRY)/ska-telescope/$(NAME) \
	--set dishmaster.image.tag=$(OCI_TAG) \
	--set global.minikube=false
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(NAME)/$(NAME):$(OCI_TAG)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	$(CUSTOM_VALUES) \
	

# include makefile targets from the submodule
-include .make/k8s.mk
-include .make/python.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/docs.mk
-include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak
